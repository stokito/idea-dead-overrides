package nl.holdyfolds.idea.plugins.inspection.deadoverrides;

import com.intellij.codeInspection.InspectionToolProvider;
import org.jetbrains.annotations.NotNull;

public class DeadOverridesProvider implements InspectionToolProvider {

    @NotNull
    @Override
    public Class[] getInspectionClasses() {
        return new Class[]{ DeadOverridesInspection.class };
    }
}
