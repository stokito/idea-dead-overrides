package nl.holdyfolds.idea.plugins.inspection.deadoverrides;

import com.intellij.codeInsight.daemon.GroupNames;
import com.intellij.codeInsight.daemon.impl.quickfix.DeleteElementFix;
import com.intellij.codeInspection.BaseJavaLocalInspectionTool;
import com.intellij.codeInspection.InspectionsBundle;
import com.intellij.codeInspection.ProblemsHolder;
import com.intellij.lang.jvm.JvmModifier;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.psi.JavaElementVisitor;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElementVisitor;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.search.searches.ClassInheritorsSearch;
import com.intellij.psi.search.searches.OverridingMethodsSearch;
import com.siyeh.ig.psiutils.MethodCallUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.function.Predicate;

/**
 * Finds methods that are considered 'dead overrides' and are likely candidates for removal/refactoring.
 *
 * A method is considered suspect by this inspection, if _all_ of the following conditions hold true:
 * - It is a non-abstract method.
 * - It is overridden in all *direct* subclasses.
 * - None of the overrides contains a call to the super-method.
 *
 * @todo: Don't use deprecated BaseJavaLocalInspectionTool.
 */
public class DeadOverridesInspection extends BaseJavaLocalInspectionTool {
    private static final Logger LOG = Logger.getInstance("#nl.holdyfolds.idea.plugins.inspection.deadoverrides.DeadOverridesInspection");

    @NotNull
    @Override
    public PsiElementVisitor buildVisitor(@NotNull final ProblemsHolder holder, boolean isOnTheFly) {
        return new JavaElementVisitor() {

            @Override
            public void visitMethod(@NotNull PsiMethod method) {
                super.visitMethod(method);

                // Abstract methods are never considered dead and are not a candidate for removal/refactoring.
                if (method.hasModifier(JvmModifier.ABSTRACT)) {
                    return;
                }

                // Skip methods that are not overridden at all.
                Collection<PsiMethod> overridingMethods = OverridingMethodsSearch.search(method).findAll();
                if (overridingMethods.isEmpty()) {
                    return;
                }

                // All *direct* subclasses must override this method.
                Collection<PsiClass> subclasses = ClassInheritorsSearch.search(method.getContainingClass(), false).findAll();
                Predicate<PsiClass> containsMethodOverride = x -> x.findMethodBySignature(method, false) != null;
                if (!subclasses.stream().allMatch(containsMethodOverride)) {
                    return;
                }

                // If no overriding method contains a super-call. The super-implementation might be a good candidate to remove/refactor.
                if (overridingMethods.stream().noneMatch(MethodCallUtils::containsSuperMethodCall)) {
                    holder.registerProblem(
                            method,
                            InspectionsBundle.message("inspection.empty.method.delete.quickfix"),
                            new DeleteElementFix(method));
                }
            }
        };
    }

    @Nullable
    @Override
    public String getStaticDescription() {
        return DeadOverridesBundle.message("inspection.staticDescription");
    }

    @NotNull
    public String getDisplayName() {
        return DeadOverridesBundle.message("inspection.displayName");
    }

    @NotNull
    public String getGroupDisplayName() {
        return GroupNames.INHERITANCE_GROUP_NAME;
    }

    @NotNull
    public String getShortName() {
        return DeadOverridesBundle.message("inspection.shortName");
    }

    public boolean isEnabledByDefault() {
        return false;
    }
}
